Feature: Create Banner Holds for a Student
 As a consumer of banner API
 I want to post information about a students
 In order to apply a hold on their account

Background:
  Given the test data is ready

Scenario: Create a hold on a persons account
  Given an empty sprhold table
  And a REST client
  And a valid token for an authorized user
  When I have a hold model with the holdCode "AA"
  And I have a hold model with the uniqueID "doej"
  And I have a hold model with the user "doej"
  And I have a hold model with the fromDate "1-JAN-2015"
  And I have a hold model with the toDate "10-JAN-2015"
  And I have a hold model with the releaseInd "N"
  And I add the hold model to the first entry of the collection
  And I construct a collection payload
  And I give the HTTP Content-type header with the value "application/json"
  And I make a POST request for /erp/holds/v1/academic
  Then the HTTP status code is 201
  And a sprhold record with sprhold_pidm 11118888 where sprhold_hldd_code equals AA does exist

Scenario: Pass multiple records to POST but the second record will fail because of invalide Hold Code
  Given a REST client
  And an empty sprhold table
  And a valid token for an authorized user
  When I have a hold model with the holdCode "AA"
  And I have a hold model with the uniqueID "doej"
  And I have a hold model with the user "doej"
  And I have a hold model with the fromDate "1-JAN-2015"
  And I have a hold model with the toDate "10-JAN-2015"
  And I have a hold model with the releaseInd "N"
  And I add the hold model to the first entry of the collection
  And I have a anotherHold model with the holdCode "AB"
  And I have a anotherHold model with the uniqueID "smithq"
  And I have a anotherHold model with the user "doej2"
  And I have a anotherHold model with the fromDate "1-FEB-2015"
  And I have a anotherHold model with the toDate "10-FEB-2015"
  And I have a anotherHold model with the releaseInd "Y"
  And I add the anotherHold model to the second entry of the collection
  And I construct a collection payload
  And I give the HTTP Content-type header with the value "application/json"
  And I make a POST request for /erp/holds/v1/academic
  Then the HTTP status code is 201
  And a sprhold record with sprhold_pidm 11118888 where sprhold_hldd_code equals AA does exist
  And a sprhold record with sprhold_pidm 11119999 where sprhold_hldd_code equals AB does not exist


Scenario: UnAuthorized user fails attempt to create a hold on a persons account
  Given an empty sprhold table
  And a REST client
  When I have a hold model with the holdCode "AA"
  And I have a hold model with the uniqueID "doej"
  And I have a hold model with the user "doej"
  And I have a hold model with the fromDate "1-JAN-2015"
  And I have a hold model with the toDate "10-JAN-2015"
  And I have a hold model with the releaseInd "N"
  And I add the hold model to the first entry of the collection
  And I construct a collection payload
  And I give the HTTP Content-type header with the value "application/json"
  And I make a POST request for /erp/holds/v1/academic
  Then the HTTP status code is 401
