Feature: Get Banner Holds for a Student
 As a consumer of banner API
 I want to get information about a students hold information
 In order to use that information somewhere else

Background:
  Given the test data is ready

Scenario: UnAuthorized user cannot get data about a user
  Given a REST client
  When I make a GET request for /erp/holds/v1/academic/?id=doej
  Then the HTTP status code is 401


Scenario: Get data about a user
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /erp/holds/v1/academic/?id=doej
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a pidm key equal to "11118888"
  And the response data element first entry contains a uniqueID key equal to "doej"


Scenario: Get data about multiple users
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /erp/holds/v1/academic/?id=doej,smithj
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a uniqueID key equal to "doej"

Scenario: Get only active holds
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /erp/holds/v1/academic/?id=doej&status=active
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a pidm key equal to "11118888"
  And the response data element first entry contains a uniqueID key equal to "doej"

Scenario: Get data with a custom date format
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /erp/holds/v1/academic/?id=doej&status=active&dateFormat=Ymd
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a pidm key equal to "11118888"
  And the response data element first entry contains a uniqueID key equal to "doej"
  And the response data element first entry contains a fromDate key equal to "20150710"
  And the response data element first entry contains a toDate key equal to "20301230"

  Scenario: Get holds by HoldCode
    Given a REST client
    And a valid token for an authorized user
    When I make a GET request for /erp/holds/v1/academic/?holdCode=AA
    Then the HTTP status code is 200
    And the HTTP Content-Type header is "application/json"
    And the response data element first entry contains a pidm key equal to "11118888"
    And the response data element first entry contains a uniqueID key equal to "doej"

  Scenario: Get active holds by HoldCode
    Given a REST client
    And a valid token for an authorized user
    When I make a GET request for /erp/holds/v1/academic/?holdCode=AA&status=active
    Then the HTTP status code is 200
    And the HTTP Content-Type header is "application/json"
    And the response data element first entry contains a pidm key equal to "11118888"
    And the response data element first entry contains a uniqueID key equal to "doej"
