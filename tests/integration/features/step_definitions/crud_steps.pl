#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

Given qr/an empty (\S+) table/, sub {
    $dbh->do(qq{ delete from $1 });
};

# a book with the title "My Book"
Given qr/a (\S+) record with the (\S+) "([^"]+)"/, sub {
    S->{'recordType'} = $1;
    S->{'record'}{$2} = $3;
};

# that the book has been added
Given qr/that the (\S+) record has been added/, sub {
    ok($1 eq S->{'recordType'}, "Create a new $1");

    if ($1 eq S->{'recordType'}) {
        my $fields = join(', ', keys %{S->{'record'}});
        my $values = join(', ', map { "'" . $_ . "'"} values %{S->{'record'}});
        my $query = "insert into " . S->{'recordType'} . '(' . $fields . ') values (' . $values . ')';

        $dbh->do($query);
    }
};
Given qr/the (\S+) of the (\S+) record with (\S+) (\S+) is selected and added to the (\S+) model as (\S+)/, sub {
    my $selectVal = $1;
    my $table = $2;
    my $field = $3;
    my $value = $4;
    my $model = $5;
    my $element = $6;

    my ($DBValue) = $dbh->selectrow_array(qq{
      select $selectVal
        from $table
        where $field = ?
    }, undef , $value);
    S->{$model.'-model'}{$element} = $DBValue;
};

Given qr/a (\S+) record with (\S+) (\S+) where (\S+) equals (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $withField = $4;
    my $withValue = $5;
    my $spec = $6;
    my $status = $6 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{
        select count(*)
            from $table
            where $field = ?
              and $withField = ?
        }, undef, $value, $withValue);

    ok($realStatus == $status, "A $table with $field of $value $spec where $withField equals $withValue");

};

Given qr/that a (\S+) record with (\S+) (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $spec = $4;
    my $status = $4 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{
        select count(*)
            from $table
            where $field = ?
        }, undef, $value);

    ok($realStatus == $status, "A $table with $field of $value $spec");

};

When qr/I have a (\S+) with the (\S+) "([^"]+)"/, sub {
    S->{'objectType'} = $1;
    S->{'object'}{$2} = $3;
};

When qr/I have a (\S+) model with the (\S+) "([^"]+)"/, sub {
    S->{'objectType'} = $1;
    S->{$1.'-model'}{$2} = $3;
};

When qr/I construct a model payload/, sub {
    S->{'object'} = {
        'dataType' => 'model',
        'data' => S->{'model'}
    }
};
When qr/I add the (\S+) model to the ([^ ]+) entry of the collection/, sub {
  my $entry = $2;
  my $index = 0;
  $index = 0 if ($entry eq 'first');
  $index = 1 if ($entry eq 'second');
  $index = 2 if ($entry eq 'third');
  $index = 3 if ($entry eq 'fourth');

  S->{'collection'}[$index] = S->{$1.'-model'};
};

When qr/I construct a collection payload/, sub {
  S->{'object'} = {
      'dataType' => 'collection',
      'data' => S->{'collection'}
  }

};

Then qr/a (\S+) record with (\S+) (\S+) where (\S+) equals (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $withField = $4;
    my $withValue = $5;
    my $spec = $6;
    my $status = $6 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{
        select count(*)
            from $table
            where $field = ?
              and $withField = ?
        }, undef, $value, $withValue);

    ok($realStatus == $status, "A $table with $field of $value $spec where $withField equals $withValue");
};

Then qr/a (\S+) record with (\S+) (\S+) (.*)/, sub {
    my $table = $1;
    my $field = $2;
    my $value = $3;
    my $spec = $4;
    my $status = $4 eq 'does exist' ? 1 : 0;

    my($realStatus) = $dbh->selectrow_array(qq{
        select count(*)
            from $table
            where $field = ?
        }, undef, $value);

    ok($realStatus == $status, "A $table with $field of $value $spec");

};
