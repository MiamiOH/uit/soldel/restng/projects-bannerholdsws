#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use lib 'lib';
use StepConfig;

Given qr/a pidm (\S+) has an active hold with code (\S+)/, sub {

    my $pidm = $1;
    my $holdCode = $2;

    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from sprhold
            where sprhold_pidm = ?
              and sprhold_hldd_code = ?
              and sprhold_from_date <= sysdate
              and sprhold_to_date > sysdate
        }, undef, $pidm, $holdCode);

    ok($realStatus == 1, "Student $pidm does have an active hold for code $holdCode");
};

Given qr/the rowID of the active hold for pidm (\S+) with hold code (\S+) is added to the hold model as internalRecordId/, sub {

    my $pidm = $1;
    my $holdCode = $2;

    my($rowId) = $dbh->selectrow_array(qq{ 
        select rowid
            from sprhold
            where sprhold_pidm = ?
              and sprhold_hldd_code = ?
              and sprhold_from_date <= sysdate
              and sprhold_to_date > sysdate
        }, undef, $pidm, $holdCode);

    S->{'hold-model'}{'internalRecordId'} = $rowId;
};

Then qr/the hold with the given rowId is no longer active/, sub {
    my($realStatus) = $dbh->selectrow_array(qq{ 
        select count(*)
            from sprhold
            where rowid = ?
              and sprhold_from_date <= sysdate
              and sprhold_to_date > sysdate
        }, undef, S->{'hold-model'}{'internalRecordId'});

    ok($realStatus == 0, "Row id " . S->{'hold-model'}{'internalRecordId'} . " is not active");
};