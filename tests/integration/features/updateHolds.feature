Feature: Update a Banner Hold for a Student
 As a consumer of banner API
 I want to remove a hold on a students account

Background:
  Given the test data is ready

Scenario: Update a hold on a persons account
  And a REST client
  And a valid token for an authorized user
  And a pidm 11118888 has an active hold with code AA
  And the rowID of the active hold for pidm 11118888 with hold code AA is added to the hold model as internalRecordId
  When I have a hold model with the holdCode "AA"
  And I have a hold model with the uniqueID "doej"
  And I have a hold model with the user "doej"
  And I have a hold model with the fromDate "1-JAN-2015"
  And I have a hold model with the toDate "20-JAN-2015"
  And I have a hold model with the releaseInd "N"
  And I add the hold model to the first entry of the collection
  And I construct a collection payload
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /erp/holds/v1/academic
  Then the HTTP status code is 200
  And the hold with the given rowId is no longer active


Scenario: UnAuthorized users are not able to update a hold on a persons account
  And a REST client
  When I have a hold model with the holdCode "AA"
  And I have a hold model with the uniqueID "doej"
  And I have a hold model with the user "doej"
  And I have a hold model with the fromDate "1-JAN-2015"
  And I have a hold model with the toDate "20-JAN-2015"
  And I have a hold model with the releaseInd "N"
  And I add the hold model to the first entry of the collection
  And I construct a collection payload
  And I give the HTTP Content-type header with the value "application/json"
  And I make a PUT request for /erp/holds/v1/academic
  Then the HTTP status code is 401
