#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw($server $authInfo $dbh);

our $server = 'http://ws/api';
our $authInfo = {
    'path' => '/authentication/v1',
    'username' => 'testLocalUser',
    'password' => 'localpass',
};

our $dbh = DBI->connect("DBI:Oracle:XE", "MUWS_GEN", "Hello123") || die DBI->errstr;

1;
