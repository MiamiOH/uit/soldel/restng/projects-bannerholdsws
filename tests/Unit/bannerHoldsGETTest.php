<?php

namespace MiamiOH\AcademicWebService\Tests\Unit;

use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\App;

include_once __DIR__ . '/../includes/sthMockStatements.php';

class bannerHoldsGET extends  TestCase
{

    private $apiUser;
    private $subject;
    private $mockRecords;

    private $peformParam = '';

    public function testGetActiveHoldsWithOutUniqueID()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('holdCode' => array('RV')));

        $this->mockRecords = array(
            array('SPRHOLD_PIDM' => 1,
                'SPRHOLD_HLDD_CODE' => 'RV',
                'SPRHOLD_USER' => 'DOEJ',
                'SPRHOLD_FROM_DATE' => '2015-02-01 09:00:00',
                'SPRHOLD_TO_DATE' => '2030-02-01 09:00:00',
                'SPRHOLD_RELEASE_IND' => 'N',
                'SPRHOLD_REASON' => NULL,
                'SPRHOLD_AMOUNT_OWED' => NULL,
                'SPRHOLD_ORIG_CODE' => NULL,
                'SPRHOLD_DATA_ORIGIN' => NULL,
                'STVHLDD_DESC' => 'Unofficial Withdrawal Advising',
                'STVHLDD_LONG_DESC' => '- Oxford students contact the Advising Office in the division of your major. Regional campus students contact the Advising Office on your regional campus.',
                'STVHLDD_REG_HOLD_IND' => 'Y',
                'ROWIDTOCHAR(ROWID)' => 'AAAGMyAAIAAAAM'
            ));

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());

    }


    public function testGetSomething()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->mockRecords = array(array('SPRHOLD_PIDM' => 1,
            'SPRHOLD_HLDD_CODE' => 'AA',
            'SPRHOLD_USER' => 'DOEJ',
            'SPRHOLD_FROM_DATE' => '2015-02-01 09:00:00',
            'SPRHOLD_TO_DATE' => '2030-02-01 09:00:00',
            'SPRHOLD_RELEASE_IND' => 'N',
            'SPRHOLD_REASON' => NULL,
            'SPRHOLD_AMOUNT_OWED' => NULL,
            'SPRHOLD_ORIG_CODE' => NULL,
            'SPRHOLD_DATA_ORIGIN' => NULL,
            'STVHLDD_DESC' => 'Unofficial Withdrawal Advising',
            'STVHLDD_LONG_DESC' => '- Oxford students contact the Advising Office in the division of your major. Regional campus students contact the Advising Office on your regional campus.',
            'STVHLDD_REG_HOLD_IND' => 'Y',
            'ROWIDTOCHAR(ROWID)' => 'AAAGMyAAIAAAAM'
        ));
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('id' => array('doej')));

        $this->mockUniqueId = 'doej';
        $this->mockPidm = 1;

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));
        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryFirstColumnWithQuery')))
            ->will($this->returnCallback(array($this, 'queryFirstColumnMock')));

        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals($payload['0']['pidm'], $this->mockPidm);
        $this->assertEquals($payload['0']['uniqueID'], $this->mockUniqueId);
        $this->assertEquals($payload['0']['holdCode'], 'AA');
        $this->assertEquals($payload['0']['internalRecordId'], $this->mockRecords['0']['ROWIDTOCHAR(ROWID)']);
    }


    public function testUnAuthorizedGetSomething()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(0);;
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('id' => array('doej')));

        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_UNAUTHORIZED, $resp->getStatus());
        $this->assertTrue(is_array($payload));
    }

    public function testGetMultipleSomethings()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->mockRecords = array(array('SPRHOLD_PIDM' => 1,
            'SPRHOLD_HLDD_CODE' => 'AA',
            'SPRHOLD_USER' => 'DOEJ',
            'SPRHOLD_FROM_DATE' => '2015-02-01 09:00:00',
            'SPRHOLD_TO_DATE' => '2030-02-01 09:00:00',
            'SPRHOLD_RELEASE_IND' => 'N',
            'SPRHOLD_REASON' => NULL,
            'SPRHOLD_AMOUNT_OWED' => NULL,
            'SPRHOLD_ORIG_CODE' => NULL,
            'SPRHOLD_DATA_ORIGIN' => NULL,
            'STVHLDD_DESC' => 'Unofficial Withdrawal Advising',
            'STVHLDD_LONG_DESC' => '- Oxford students contact the Advising Office in the division of your major. Regional campus students contact the Advising Office on your regional campus.',
            'STVHLDD_REG_HOLD_IND' => 'Y',
            'ROWIDTOCHAR(ROWID)' => 'AAAGMyAAIAAAAM'
        ),
            array('SPRHOLD_PIDM' => 1,
                'SPRHOLD_HLDD_CODE' => 'AB',
                'SPRHOLD_USER' => 'DOEJ',
                'SPRHOLD_FROM_DATE' => '2014-02-01 09:00:00',
                'SPRHOLD_TO_DATE' => '2025-02-01 09:00:00',
                'SPRHOLD_RELEASE_IND' => 'N',
                'SPRHOLD_REASON' => NULL,
                'SPRHOLD_AMOUNT_OWED' => NULL,
                'SPRHOLD_ORIG_CODE' => NULL,
                'SPRHOLD_DATA_ORIGIN' => NULL,
                'STVHLDD_DESC' => 'Unofficial Withdrawal Advising',
                'STVHLDD_LONG_DESC' => '- Oxford students contact the Advising Office in the division of your major. Regional campus students contact the Advising Office on your regional campus.',
                'STVHLDD_REG_HOLD_IND' => 'Y',
                'ROWIDTOCHAR(ROWID)' => 'AAAGMyAAIAAAAM'
            )
        );
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('id' => array('doej')));
        $this->mockUniqueId = 'doej';
        $this->mockPidm = 1;

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));
        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryFirstColumnWithQuery')))
            ->will($this->returnCallback(array($this, 'queryFirstColumnMock')));


        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals($payload['0']['pidm'], $this->mockPidm);
        $this->assertEquals($payload['0']['uniqueID'], $this->mockUniqueId);
        $this->assertEquals($payload['0']['holdCode'], 'AA');
        $this->assertEquals($payload['1']['pidm'], $this->mockPidm);
        $this->assertEquals($payload['1']['uniqueID'], $this->mockUniqueId);
        $this->assertEquals($payload['1']['holdCode'], 'AB');
    }

    public function testGetDateFormat()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $dateFormat = 'Ymd';

        $this->mockRecords = array(array('SPRHOLD_PIDM' => 1,
            'SPRHOLD_HLDD_CODE' => 'AA',
            'SPRHOLD_USER' => 'DOEJ',
            'SPRHOLD_FROM_DATE' => '2015-02-01 09:00:00',
            'SPRHOLD_TO_DATE' => '2030-02-01 09:00:00',
            'SPRHOLD_RELEASE_IND' => 'N',
            'SPRHOLD_REASON' => NULL,
            'SPRHOLD_AMOUNT_OWED' => NULL,
            'SPRHOLD_ORIG_CODE' => NULL,
            'SPRHOLD_DATA_ORIGIN' => NULL,
            'STVHLDD_DESC' => 'Unofficial Withdrawal Advising',
            'STVHLDD_LONG_DESC' => '- Oxford students contact the Advising Office in the division of your major. Regional campus students contact the Advising Office on your regional campus.',
            'STVHLDD_REG_HOLD_IND' => 'Y',
            'ROWIDTOCHAR(ROWID)' => 'AAAGMyAAIAAAAM'
        ));

        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('id' => array('doej'), 'dateFormat' => $dateFormat));

        $this->mockUniqueId = 'doej';
        $this->mockPidm = 1;

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));
        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryFirstColumnWithQuery')))
            ->will($this->returnCallback(array($this, 'queryFirstColumnMock')));

        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals($this->mockPidm, $payload['0']['pidm']);
        $this->assertEquals($this->mockUniqueId, $payload['0']['uniqueID']);
        $this->assertEquals('AA', $payload['0']['holdCode']);
        $this->assertEquals('20150201', $payload['0']['fromDate']);
        $this->assertEquals('20300201', $payload['0']['toDate']);

    }

    public function testGetActive()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->mockRecords = array(
            array('SPRHOLD_PIDM' => 1,
                'SPRHOLD_HLDD_CODE' => 'AA',
                'SPRHOLD_USER' => 'DOEJ',
                'SPRHOLD_FROM_DATE' => '20140101 09:00:00',
                'SPRHOLD_TO_DATE' => '20301212 09:00:00',
                'SPRHOLD_RELEASE_IND' => 'N',
                'SPRHOLD_REASON' => NULL,
                'SPRHOLD_AMOUNT_OWED' => NULL,
                'SPRHOLD_ORIG_CODE' => NULL,
                'SPRHOLD_DATA_ORIGIN' => NULL,
                'STVHLDD_DESC' => 'Unofficial Withdrawal Advising',
                'STVHLDD_LONG_DESC' => '- Oxford students contact the Advising Office in the division of your major. Regional campus students contact the Advising Office on your regional campus.',
                'STVHLDD_REG_HOLD_IND' => 'Y',
                'ROWIDTOCHAR(ROWID)' => 'AAAGMyAAIAAAAM'
            ),
        );

        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('id' => array('doej'), 'status' => 'active'));


        $this->mockUniqueId = 'doej';
        $this->mockPidm = 1;

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));
        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryFirstColumnWithQuery')))
            ->will($this->returnCallback(array($this, 'queryFirstColumnMock')));

        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertEquals(1, count($payload));

    }

    public function testGetNoRecords()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn(array('id' => array('-1')));

        $this->mockRecords = array();

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->subject->setRequest($this->request);
        $resp = $this->subject->getHoldList();

        $payload = $resp->getPayload();
        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals(sizeof($payload), 0);

    }

    public function locationForNameWith($subject)
    {
        $this->locationForNameBeingCalled = $subject;
        return true;
    }

    public function locationForParamsWith($subject)
    {
        $this->locationForParamsBeingCalled = $subject;
        return true;
    }

    public function locationForMock()
    {
        $path = str_replace('.', '/', $this->locationForNameBeingCalled);
        $params = $this->locationForParamsBeingCalled;
        $qsFunction = function ($key) use ($params) {
            return $key . '=' . $params[$key];
        };
        $queryString = implode('&', array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));
        return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
    }

    public function performSqlWith($subject)
    {
        return true;
    }

    public function performParamsWith($subject)
    {
        $this->performParam = $subject;
        return true;
    }

    public function performMock()
    {
        return true;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->querySql = $subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->mockRecords;
    }

    public function queryFirstColumnWithQuery($subject)
    {
        $this->querySql = $subject;
        return true;
    }

    public function queryFirstColumnMock()
    {
        if (strpos($this->querySql, 'lower')) {
            return $this->mockUniqueId;
        }
        if (strpos($this->querySql, 'upper')) {
            return $this->mockPidm;
        }
    }

    protected function setUp()
    {

        $this->performParam = '';

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstcolumn', 'perform', 'prepare', 'getDBConnection', 'queryall_array'))
            ->getMock();

        $this->dbh->method('perform')
            ->with($this->callback(array($this, 'performSqlWith')), $this->callback(array($this, 'performParamsWith')))
            ->will($this->returnCallback(array($this, 'performMock')));


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $api->method('locationFor')
            ->with($this->callback(array($this, 'locationForNameWith')), $this->callback(array($this, 'locationForParamsWith')))
            ->will($this->returnCallback(array($this, 'locationForMock')));

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('bind_by_name', 'execute'))
            ->getMock();

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $this->subject = new \MiamiOH\ProjectsBannerholdsws\Services\Academic();
        $this->subject->setDatabase($db);
        $this->subject->setApiUser($this->apiUser);

    }

}
