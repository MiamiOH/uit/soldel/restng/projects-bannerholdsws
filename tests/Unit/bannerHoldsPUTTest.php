<?php

use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\App;

include_once __DIR__ . '/../includes/sthMockStatements.php';

class bannerHoldsPUT extends TestCase
{
    private $subject;
    private $apiUser;

    private $bindPlaceHolder = '';
    private $boundValues = array();

    protected function setUp()
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = array();

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstcolumn', 'perform', 'prepare'))
            ->getMock();

        $this->dbh->db = new stdClass();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $api->method('locationFor')
            ->with($this->callback(array($this, 'locationForNameWith')), $this->callback(array($this, 'locationForParamsWith')))
            ->will($this->returnCallback(array($this, 'locationForMock')));

        $this->subject = new MiamiOH\ProjectsBannerholdsws\Services\Academic();
        $this->subject->setDatabase($db);
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('bind_by_name', 'execute'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_namePhWith')), $this->callback(array($this, 'bind_by_nameValueWith')))
            ->willReturn(true);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $this->dbh->method('prepare')->willReturn($this->sth);

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        $this->subject = new MiamiOH\ProjectsBannerholdsws\Services\Academic();
        $this->subject->setDatabase($db);
        $this->subject->setApiUser($this->apiUser);

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage MiamiOH\ProjectsBannerholdsws\Services\Academic::updateUserHold requires internalRecordId in data body
     */
    public function testMissinginternalRecordId()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->request->expects($this->once())->method('getData')->willReturn(
            array('data' => array(
                array(
                    'uniqueID' => 'DOEJ',
                    'holdCode' => 'WA',
                    'user' => 'doej',
                    'fromDate' => '07/10/15',
                    'toDate' => '12/30/30',
                    'releaseInd' => 'N'
                )
            )
            )
        );
        $this->subject->setRequest($this->request);
        $resp = $this->subject->updateUserHold();

    }

    public function testbannerHoldsUnAuthorizedPUT()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(0);

        $this->request->expects($this->once())->method('getData')->willReturn(
            array('data' => array(
                array(
                    'pidm' => 1111888,
                    'uniqueID' => 'doej',
                    'holdCode' => 'WA',
                    'user' => 'smithj',
                    'fromDate' => '07/10/15',
                    'toDate' => '12/30/30',
                    'releaseInd' => 'N',
                    'internalRecordId' => 'AAAGMyAAIAAAAM9AAK'
                )
            )
            )
        );
        $this->subject->setRequest($this->request);
        $resp = $this->subject->updateUserHold();
        $payload = $resp->getPayload();

        $this->assertEquals(App::API_UNAUTHORIZED, $resp->getStatus());

    }


    public function testUpdateHold()
    {
        $this->apiUser->expects($this->once())->method('isAuthorized')->willReturn(1);

        $this->request->expects($this->once())->method('getData')->willReturn(
            array('data' => array(
                array(
                    'pidm' => 1111888,
                    'uniqueID' => 'doej',
                    'holdCode' => 'WA',
                    'user' => 'smithj',
                    'fromDate' => '07/10/15',
                    'toDate' => '12/30/30',
                    'releaseInd' => 'N',
                    'internalRecordId' => 'AAAGMyAAIAAAAM9AAK'
                )
            )
            )
        );
        $this->subject->setRequest($this->request);
        $resp = $this->subject->updateUserHold();
        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        $this->assertEquals('07/10/2015', $this->boundValues[':P_FROM_DATE']);
        $this->assertEquals('12/30/2030', $this->boundValues[':P_TO_DATE']);

    }

    public function locationForNameWith($subject)
    {
        $this->locationForNameBeingCalled = $subject;
        return true;
    }

    public function locationForParamsWith($subject)
    {
        $this->locationForParamsBeingCalled = $subject;
        return true;
    }

    public function locationForMock()
    {
        $path = str_replace('.', '/', $this->locationForNameBeingCalled);
        $params = $this->locationForParamsBeingCalled;
        $qsFunction = function ($key) use ($params) {
            return $key . '=' . $params[$key];
        };
        $queryString = implode('&', array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));
        return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
    }

    public function bind_by_namePhWith($subject)
    {
        $this->bindPlaceHolder = $subject;
        return true;
    }

    public function bind_by_nameValueWith($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';
        return true;
    }

}
