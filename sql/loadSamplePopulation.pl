#!/usr/bin/perl

use strict;

use DBI;
use Data::Dumper;
use LWP::UserAgent;
use JSON;
use Getopt::Long;

my $studentData = loadDataFromFile('students');
my $holdData = loadDataFromFile('holds');
my $personData = loadDataFromFile('personIds');
my $validateHoldData = loadDataFromFile('validateHolds');



my $dbh = DBI->connect("DBI:Oracle:XE", "system", "manager") || die DBI->errstr;

foreach my $table (qw(szbuniq sprhold spriden stvhldd)) {
    $dbh->do(qq{
            delete from $table
        });
}

foreach my $hold (@{$holdData}) {
  $dbh->do(q{
      insert into sprhold (sprhold_pidm, sprhold_hldd_code, sprhold_user, sprhold_from_date, sprhold_to_date, sprhold_release_ind,sprhold_activity_date)
          values (?,?,?,to_date(?),to_date(?),?,sysdate)
      }, undef, $hold->{'pidm'}, uc $hold->{'hlddCode'}, $hold->{'user'},	$hold->{'fromDate'},$hold->{'toDate'}, $hold->{'releaseInd'});

}

foreach my $person (@{$personData}) {

    $dbh->do(q{
        insert into spriden (spriden_pidm, spriden_id, spriden_first_name, spriden_last_name, spriden_change_ind)
            values (?, ?, ?, ?, ?)
        }, undef, $person->{'pidm'}, $person->{'id'}, $person->{'firstName'}, $person->{'lastName'}, $person->{'changeInd'});
}

foreach my $validateHold (@{$validateHoldData}) {

    $dbh->do(q{
        insert into stvhldd (STVHLDD_CODE, STVHLDD_REG_HOLD_IND, stvhldd_desc, STVHLDD_APPLICATION_HOLD_IND, STVHLDD_COMPLIANCE_HOLD_IND, stvhldd_activity_date)
            values (?, ?, ?, ?, ?, sysdate)
        }, undef, $validateHold->{'code'}, $validateHold->{'regHold'}, $validateHold->{'desc'}, $validateHold->{'appHoldInd'}, $validateHold->{'complianceHold'});
}


foreach my $student (@{$studentData}) {

    $dbh->do(q{
        insert into szbuniq (szbuniq_banner_id, szbuniq_unique_id, szbuniq_pidm)
            values (?, upper(?), ?)
        }, undef, $student->{'bannerId'}, uc $student->{'uniqueId'}, $student->{'pidm'});
}



sub loadDataFromFile {
    my $fileName = shift;

    my $data = [];
    open FILE, "../sql/sampleData/$fileName.txt" or die "Couldn't open sampleData/$fileName.txt: $!";

    my $names = <FILE>;
    chomp $names;

    my @fieldNames = split("\t", $names);

    while (<FILE>) {
        chomp;
        my @values = split("\t");

        my $record = {};
        for (my $i = 0; $i < scalar(@fieldNames); $i++) {
            $record->{$fieldNames[$i]} = $values[$i];
        }
        push(@{$data}, $record);
    }

    close FILE;

    return $data;
}
