SET TERMOUT OFF

CREATE ROLE MUWS_GEN_RL;
create user MUWS_GEN identified by Hello123;

grant create session to MUWS_GEN_RL;
grant MUWS_GEN_RL to MUWS_GEN;
