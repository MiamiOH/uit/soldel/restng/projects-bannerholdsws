declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

begin

  v_authman_app_name := 'erpApi';
  v_authman_module_name := 'Holds';
  v_authman_entity_name := 'testLocalUser';
  v_authman_grantkey := 'update';

  -- Add Authman application. Do this only once.

v_authman_grantkey := 'update';

  -- Add the first AuthMan grantkey. Do this only once per grantkey.
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

v_authman_grantkey := 'select';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

v_authman_grantkey := 'create';

  -- Add the first AuthMan grantkey. Do this only once per grantkey.
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

end;
/
