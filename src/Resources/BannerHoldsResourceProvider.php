<?php

namespace MiamiOH\ProjectsBannerholdsws\Resources;

use MiamiOH\RESTng\Util\ResourceProvider;

class BannerHoldsResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'ERP\Holds\Academic',
            'class' => 'MiamiOH\ProjectsBannerholdsws\Services\Academic',
            'description' => 'Exposing the Banner Holds API.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'erp.holds.v1.academic.get',
            'description' => 'Get the banner holds for a student',
            'pattern' => '/erp/holds/v1/academic/',
            'service' => 'ERP\Holds\Academic',
            'method' => 'getHoldList',
            'returnType' => 'collection',
            'options' => array(
                'id' => array('type' => 'list', 'description' => 'List of UniqueIDs or Banner IDs'),
                'dateFormat' => array('description' => 'PHP date() compatible date format for outputting dates'),
                'status' => array('description' => 'Hold status filter (active)'),
                'holdCode' => array('type' => 'list', 'description' => 'List of Hold Codes'),
            ),
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'erp.holds.v1.academic.post',
            'description' => 'Create a banner hold for a student',
            'pattern' => '/erp/holds/v1/academic',
            'service' => 'ERP\Holds\Academic',
            'method' => 'createUserHold',
            'returnType' => 'none',
            'middleware' => array(
                'authenticate' => array(),
            ),

        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'erp.holds.v1.academic.put',
            'description' => 'Create a banner hold for a student',
            'pattern' => '/erp/holds/v1/academic',
            'service' => 'ERP\Holds\Academic',
            'method' => 'updateUserHold',
            'returnType' => 'none',
            'middleware' => array(
                'authenticate' => array(),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}