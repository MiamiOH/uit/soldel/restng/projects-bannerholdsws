<?php

namespace MiamiOH\ProjectsBannerholdsws\Services;

use MiamiOH\RESTng\Legacy\DB\STH;

class Academic extends \MiamiOH\RESTng\Service
{
    private $dbDataSourceName = 'MUWS_GEN_PROD';
    private $dbh;

    private $oracleDateFormat = 'YYYY-MM-DD HH24:MI:SS';
    private $phpDateFormat = 'Y-m-d H:i:s';

    private $fetchHoldStatus = '';

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function getHoldList()
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $user = $this->getApiUser();
        $response = $this->getResponse();
        $authorized = $user->isAuthorized('erpApi', 'Holds', 'update');


        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        $ids = '';

        if (isset($options['id']) && $options['id']) {
            $ids = $options['id'];
        } else {
            $ids = '';
        }

        if (isset($options['dateFormat'])) {
            $this->phpDateFormat = $options['dateFormat'];
        }

        if (isset($options['status']) && !in_array($options['status'], array('active'))) {
            throw new \Exception('Invalid status "' . $options['status'] . '" for holds.');
        }

        if (isset($options['status'])) {
            $this->fetchHoldStatus = $options['status'];
        }

        if (isset($options['holdCode'])) {
            $this->holdCode = $options['holdCode'];
        } else {
            $this->holdCode = '';
        }

        $this->dbh->perform('
        ALTER SESSION SET NLS_DATE_FORMAT = ?
      ', $this->oracleDateFormat);

        $recordResults = array();
        $filterParms = array(
            'holdCode' => $this->holdCode,
            'holdStatus' => $this->fetchHoldStatus,
            'ids' => $ids
        );
        $recordResults = $this->queryHoldRecords($filterParms);


        $response->setPayload($recordResults);
        return $response;
    }

    protected function queryHoldRecords($filterValues)
    {

        $values = array();
        $conditions = array();
        $queryConditionString = '';

        //Should have holdCode, holdStatus & ids in filterValues
        $query = "SELECT '' || sprhold.rowid,
                    sprhold.sprhold_pidm,
                    sprhold.sprhold_hldd_code,
                    sprhold.sprhold_user,
                    sprhold.sprhold_from_date,
                    sprhold.sprhold_to_date,
                    sprhold.sprhold_release_ind,
                    sprhold.sprhold_reason,
                    sprhold.sprhold_amount_owed,
                    sprhold.sprhold_orig_code,
                    sprhold.sprhold_data_origin,
                    stvhldd_add.stvhldd_desc,
                    stvhldd_add.stvhldd_long_desc,
                    stvhldd_add.stvhldd_reg_hold_ind,
                    rowidtochar(sprhold.ROWID) as row_id
                FROM sprhold,stvhldd_add
                WHERE 1=1
                AND sprhold.sprhold_hldd_code = stvhldd_add.stvhldd_code";

        if (isset($filterValues['holdCode']) && $filterValues['holdCode']) {
            $placeHolders = array();
            foreach ($filterValues['holdCode'] as $holdCode) {
                $placeHolders[] = 'upper(?)';
                $values[] = $holdCode;
            }
            $conditions[] = 'SPRHOLD_HLDD_CODE in (' . join(', ', $placeHolders) . ')';
        }
        if (isset($filterValues['holdStatus']) && $filterValues['holdStatus']) {
            if ($filterValues['holdStatus'] === 'active') {
                $conditions[] = 'SPRHOLD_FROM_DATE  <= sysdate ';
                $conditions[] = 'SPRHOLD_TO_DATE  > sysdate ';
            }
        }
        if (isset($filterValues['ids']) && $filterValues['ids']) {
            $placeHolders = array();
            foreach ($filterValues['ids'] as $id) {
                $placeHolders[] = '?';
                if($this->isBannerId($id)) {
                    $values[] = $this->getPidmByBannerId($id);
                } else { // unique ID
                    $values[] = $this->getPidmByUniqueId($id);
                }
            }
            $conditions[] = 'SPRHOLD_PIDM in (' . join(',', $placeHolders) . ')';
        }


        if ($conditions) {
            $queryConditionString = ' and ' . join(' and ', $conditions);
        }

        $query .= $queryConditionString;

        $data = $this->dbh->queryall_array($query, $values);
        $payloadSet = [];
        foreach ($data as $rowId => $row) {
            if (is_array($row)) {
                $row = array_change_key_case($row, CASE_UPPER);
                $record = $this->buildRecord(
                    $row,
                    $this->getUniqueIdByPidm($row['SPRHOLD_PIDM']),
                    $this->getBannerIdByPidm($row['SPRHOLD_PIDM'])
                );
                $payloadSet[] = $record;
            }
        }

        return $payloadSet;
    }

    private function isBannerId(string $id): bool {
        return preg_match('/\+\d{6,}/', $id);
    }

    protected function getPidmByUniqueId($uniqueID)
    {
        $pidm = $this->dbh->queryfirstcolumn('
          select szbuniq_pidm from szbuniq where upper(szbuniq_unique_id) = upper(?)
        ', $uniqueID);
        return $pidm;
    }

    protected function getPidmByBannerId($id)
    {
        $pidm = $this->dbh->queryfirstcolumn('
          select spriden_pidm from spriden where spriden_id = ?
        ', $id);
        return $pidm;
    }

    protected function getUniqueIdByPidm($pidm)
    {
        $uniqueId = $this->dbh->queryfirstcolumn('
          select lower(szbuniq_unique_id) from szbuniq where szbuniq_pidm = ?
        ', $pidm);
        return $uniqueId;
    }

    private function getBannerIdByPidm($pidm)
    {
        return $this->dbh->queryfirstcolumn('
          select spriden_id from spriden where spriden_pidm = ?
        ', $pidm);
    }

    protected function buildRecord($row, $uniqueID, $bannerId)
    {
        $toDateStamp = strtotime($row['SPRHOLD_TO_DATE']);
        $fromDateStamp = strtotime($row['SPRHOLD_FROM_DATE']);

        $record = array(
            'pidm' => $row['SPRHOLD_PIDM'],
            'uniqueID' => $uniqueID,
            'bannerId' => $bannerId,
            'holdCode' => $row['SPRHOLD_HLDD_CODE'],
            'user' => $row['SPRHOLD_USER'],
            'fromDate' => date($this->phpDateFormat, $fromDateStamp),
            'toDate' => date($this->phpDateFormat, $toDateStamp),
            'releaseInd' => $row['SPRHOLD_RELEASE_IND'],
            'reason' => $row['SPRHOLD_REASON'],
            'amountOwed' => $row['SPRHOLD_AMOUNT_OWED'],
            'originCode' => $row['SPRHOLD_ORIG_CODE'],
            'dataOrigin' => $row['SPRHOLD_DATA_ORIGIN'],
            'sortDesc' => $row['STVHLDD_DESC'],
            'longDesc' => $row['STVHLDD_LONG_DESC'],
            'regHoldInd' => $row['STVHLDD_REG_HOLD_IND'],
            'internalRecordId' => $row['ROW_ID']
        );
        return $record;
    }

    public function createUserHold()
    {
        $request = $this->getRequest();
        $payload = $request->getData();
        $user = $this->getApiUser();
        $response = $this->getResponse();
        $authorized = $user->isAuthorized('erpApi', 'Holds', 'update');

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        $results = array();

        foreach ($payload['data'] as $data) {
            if ((!(isset($data['uniqueID']) && $data['uniqueID'])
                && !(isset($data['bannerId']) && $data['bannerId'])) || (isset($data['uniqueID']) && isset($data['bannerId']) && $data['uniqueID'] && $data['bannerId'])) {
                throw new \Exception(__CLASS__ . '::createUserHold requires uniqueID or bannerId in data body, but not both');
            }

            if (!(isset($data['holdCode']) && $data['holdCode'])) {
                throw new \Exception(__CLASS__ . '::createUserHold requires holdCode in data body');
            }

            if (!(isset($data['user']) && $data['user'])) {
                throw new \Exception(__CLASS__ . '::createUserHold requires user in data body');
            }

            if (!(isset($data['fromDate']) && $data['fromDate'])) {
                $data['fromDateFinal'] = date("m/d/Y");
            } else {
                $data['fromDateFinal'] = date('m/d/Y', strtotime($data['fromDate']));
                if ($data['fromDateFinal'] === false) {
                    throw new \Exception(__CLASS__ . '::createUserHold could not parse fromDate');
                }
            }

            if (!(isset($data['toDate']) && $data['toDate'])) {
                $data['toDateFinal'] = '12/31/2099';
            } else {
                $data['toDateFinal'] = date('m/d/Y', strtotime($data['toDate']));
                if ($data['toDateFinal'] === false) {
                    throw new \Exception(__CLASS__ . '::createUserHold could not parse toDate');
                }
            }

            if (!(isset($data['releaseInd']) && $data['releaseInd'])) {
                $data['releaseInd'] = 'N';
            }

            if (!is_array($data)) {
                throw new \Exception('Data for createUserHold must be an array');
            }

            try {
                $returnedRowID = $this->createUserHoldRecord($data);
                $results[strtoupper((isset($data['uniqueID']) ? $data['uniqueID'] : $data['bannerId']) . '.' . $data['holdCode'])] =
                    array('internalRecordId' => $returnedRowID, 'code' => \MiamiOH\RESTng\App::API_CREATED, 'message' => '');
            } catch (\Exception $e) {
                $results[strtoupper((isset($data['uniqueID']) ? $data['uniqueID'] : $data['bannerId']) . '.' . $data['holdCode'])] =
                    array('internalRecordId' => $returnedRowID, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => $e->getMessage());
            }

            $returnedRowID = '';

        }
        $response->setPayload($results);
        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        return $response;
    }

    protected function createUserHoldRecord($data)
    {
        // PIDM Needed to call Banner API's
        $data['pidm'] = isset($data['uniqueID'])
            ? $this->getPidmByUniqueId($data['uniqueID'])
            : $this->getPidmByBannerId($data['bannerId']);
        if ($data['pidm'] === DB_EMPTY_SET) {
            throw new \Exception('Invalid UniqueID provided');
        } else {
            //Variable initialization
            $rowID = '';

            $query = "
      declare
        BEGIN
          GB_HOLD.P_CREATE(
            P_PIDM => :P_PIDM,
            P_HLDD_CODE => :P_HLDD_CODE,
            P_USER => :P_USER,
            P_FROM_DATE => to_date(:P_FROM_DATE, 'MM/DD/YYYY HH24:MI:SS'),
            P_TO_DATE => to_date(:P_TO_DATE, 'MM/DD/YYYY HH24:MI:SS'),
            P_RELEASE_IND => :P_RELEASE_IND,
            P_REASON => :P_REASON,
            P_AMOUNT_OWED => :P_AMOUNT_OWED,
            P_ORIG_CODE => :P_ORIG_CODE,
            P_DATA_ORIGIN => :P_DATA_ORIGIN,
            P_ROWID_OUT => :P_ROWID_OUT
          );
        END;
      ";

            $reason = isset($data['reason']) ? $data['reason'] : null;
            $amountOwed = isset($data['amtOwed']) ? $data['amtOwed'] : null;
            $origCode = isset($data['origCode']) ? $data['origCode'] : null;
            $dataOrigin = isset($data['dataOrigin']) ? $data['dataOrigin'] : null;

            $sth = $this->dbh->prepare($query);
            $sth->bind_by_name(':P_PIDM', $data['pidm']);
            $sth->bind_by_name(':P_HLDD_CODE', $data['holdCode']);
            $sth->bind_by_name(':P_USER', $data['user']);
            $sth->bind_by_name(':P_FROM_DATE', $data['fromDateFinal']);
            $sth->bind_by_name(':P_TO_DATE', $data['toDateFinal']);
            $sth->bind_by_name(':P_RELEASE_IND', $data['releaseInd']);
            $sth->bind_by_name(':P_REASON', $reason);
            $sth->bind_by_name(':P_AMOUNT_OWED', $amountOwed);
            $sth->bind_by_name(':P_ORIG_CODE', $origCode);
            $sth->bind_by_name(':P_DATA_ORIGIN', $dataOrigin);
            $sth->bind_by_name(':P_ROWID_OUT', $rowID, 100);
            $sth->execute();
            return $rowID;
        }
    }

    public function updateUserHold()
    {
        $request = $this->getRequest();
        $payload = $request->getData();
        $user = $this->getApiUser();
        $response = $this->getResponse();
        $authorized = $user->isAuthorized('erpApi', 'Holds', 'update');

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }


        $results = array();
        foreach ($payload['data'] as $data) {
            if (!(isset($data['internalRecordId']) && $data['internalRecordId'])) {
                throw new \Exception(__CLASS__ . '::updateUserHold requires internalRecordId in data body');
            }
            try {
                $this->updateUserHoldRecord($data);
                $results[strtoupper((isset($data['uniqueID']) ? $data['uniqueID'] : $data['bannerId']) . '.' . $data['holdCode'])] =
                    array('code' => \MiamiOH\RESTng\App::API_OK, 'message' => '');
            } catch (\Exception $e) {
                $results[strtoupper((isset($data['uniqueID']) ? $data['uniqueID'] : $data['bannerId']) . '.' . $data['holdCode'])] =
                    array('code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => $e->getMessage());
            }
        }
        $response->setPayload($results);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }

    public function updateUserHoldRecord($data)
    {
        // PIDM Needed to call Banner API's
        $data['pidm'] = isset($data['uniqueID'])
            ? $this->getPidmByUniqueId($data['uniqueID'])
            : $this->getPidmByBannerId($data['bannerId']);

        if ($data['pidm'] === DB_EMPTY_SET) {
            throw new \Exception('Invalid UniqueID/BannerId provided');
        } else {
            if (isset($data['fromDate'])) {
                $data['fromDateFinal'] = date('m/d/Y', strtotime($data['fromDate']));
                if ($data['fromDateFinal'] === false) {
                    throw new \Exception(__CLASS__ . '::createUserHold could not parse fromDate');
                }
            }

            if (isset($data['toDate'])) {
                $data['toDateFinal'] = date('m/d/Y', strtotime($data['toDate']));
                if ($data['toDateFinal'] === false) {
                    throw new \Exception(__CLASS__ . '::createUserHold could not parse toDate');
                }
            }

            //Variable initialization
            $query = "
      declare
        BEGIN
          GB_HOLD.P_UPDATE(
            P_PIDM => :P_PIDM,
            P_HLDD_CODE => :P_HLDD_CODE,
            P_USER => :P_USER,
            P_FROM_DATE => to_date(:P_FROM_DATE, 'MM/DD/YYYY HH24:MI:SS'),
            P_TO_DATE => to_date(:P_TO_DATE, 'MM/DD/YYYY HH24:MI:SS'),
            P_RELEASE_IND => :P_RELEASE_IND,
            P_REASON => :P_REASON,
            P_AMOUNT_OWED => :P_AMOUNT_OWED,
            P_ORIG_CODE => :P_ORIG_CODE,
            P_DATA_ORIGIN => :P_DATA_ORIGIN,
            P_ROWID => :P_ROWID
          );
        END;
      ";
            $reason = isset($data['reason']) ? $data['reason'] : null;
            $amountOwed = isset($data['amtOwed']) ? $data['amtOwed'] : null;
            $origCode = isset($data['origCode']) ? $data['origCode'] : null;
            $dataOrigin = isset($data['dataOrigin']) ? $data['dataOrigin'] : null;

            /** @var STH $sth */
            $sth = $this->dbh->prepare($query);
            $sth->bind_by_name(':P_PIDM', $data['pidm']);
            $sth->bind_by_name(':P_HLDD_CODE', $data['holdCode']);
            $sth->bind_by_name(':P_USER', $data['user']);
            $sth->bind_by_name(':P_FROM_DATE', $data['fromDateFinal']);
            $sth->bind_by_name(':P_TO_DATE', $data['toDateFinal']);
            $sth->bind_by_name(':P_RELEASE_IND', $data['releaseInd']);
            $sth->bind_by_name(':P_REASON', $reason);
            $sth->bind_by_name(':P_AMOUNT_OWED', $amountOwed);
            $sth->bind_by_name(':P_ORIG_CODE', $origCode);
            $sth->bind_by_name(':P_DATA_ORIGIN', $dataOrigin);
            $sth->bind_by_name(':P_ROWID', $data['internalRecordId']);
            $sth->execute();
        }
    }
}
