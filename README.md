# Banner Hold RESTful Web Service

## Description

projects-bannerHoldsWs has been converted into RESTng 2.0. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0 requirements. No functionality or logic change were made.  After conversion was done, PHPUnit has been executed. It failed PHPUnit test due to undefined variables found. Noted that no fix being made to unit test code. 

## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/erp

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.

## Wiki

https://miamioh.atlassian.net/wiki/spaces/eis/pages/3232072573/Holds+Audit+Trail+and+Reporting

## TD Asset

https://miamioh.teamdynamix.com/TDNext/Apps/741/Assets/AssetDet?AssetID=49868